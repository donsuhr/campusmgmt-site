module.exports = function copyConfig(grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: options.app,
                    dest: options.dist,
                    src: [
                        '_redirects',
                    ],
                },
            ],
        },
    };
};
