const timeGrunt = require('time-grunt');
const loadGruntConfig = require('load-grunt-config');
const path = require('path');

module.exports = function gruntfile(grunt) {
    const config = {
        app: 'app',
        dist: 'dist',
        sassOutput: '.tmp/sass-out/',
        livereloadPort: 35729,
        testLiveReload: 35730,
    };

    timeGrunt(grunt);

    loadGruntConfig(grunt, {
        data: config,
        configPath: path.join(process.cwd(), '/build/grunt'),
    });

    grunt.registerTask('build', [
        'clean',
        'copy',
    ]);

    grunt.registerTask('default', [
        'build',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);

    grunt.registerTask('netlify', [
        'build',
    ]);
};
